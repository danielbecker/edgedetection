package edgedetection;

import braggfinderlibrary.BraggSpot;
import braggfinderlibrary.BraggSpotList;
import braggfinderlibrary.Constants;
import static braggfinderlibrary.Constants.KERNEL_3;
import braggfinderlibrary.DataHandling;
import braggfinderlibrary.FileHandling;
import java.util.LinkedList;

/**
 *
 * @author Daniel Becker <beckerd@htw-berlin.de>
 */
public class EdgeDetection {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        String fileName = "/Users/db/git/bragg_finder/Data2/CatB/data/LCLS_2013_Jun29_r0191_235338_64ad.h5";
        String fileNameList = "/Users/db/git/experiment_data/5HT2B/good_10_bad_10.txt";
        String geometryFileName = "/Users/db/git/bragg_finder/Data2/5HT2B/geom.h5";
        String averageFileName = "/Users/db/git/bragg_finder/Data2/5HT2B/average.h5";
        String outputFolder = "/Users/db/Desktop/2014-11-09/5ht2b/";
        
        LinkedList<String> fileNames = FileHandling.loadFilenamesFromListFile(fileNameList);
        long[][][] geometry = FileHandling.readGeometry(geometryFileName);
        short[][]average = FileHandling.readProcessedDataFromH5(averageFileName);

        short counter = 1;
        for (String fileName: fileNames){
            System.out.println("Processing " + counter + "/" + fileNames.size());
            short[][] data = FileHandling.readDataFromH5(fileName);
            data = DataHandling.normalizeData(data);
            data = DataHandling.applyGeometryToData(data, geometry);
            data = DataHandling.subtractData(data, average);
            data = DataHandling.applyKernelToData(KERNEL_3, data);
            data = DataHandling.applyEdgeDetection(data);
            
            String singleFileName = fileName.split("/")[fileName.split("/").length-1];
            
            FileHandling.writeDataAsImage(data, outputFolder + singleFileName + "_raw.png"); 
            data = DataHandling.findClustersImproved(data);
            
            BraggSpotList braggSpotList = DataHandling.findBraggSpots(data,0);
                
            short[][] clusters = new short[Constants.DIMENSION][Constants.DIMENSION];
            for (int i = 0; i < braggSpotList.getBraggSpotCount(); i++) {
                if (braggSpotList.getBraggSpot(i).getSize() < 1000) {
                    for (short[] pixel: braggSpotList.getBraggSpot(i).getPixels()){
                        clusters[pixel[1]][pixel[0]] = (short) pixel[2];
                    }
                }
            }

            FileHandling.writeDataAsImage(clusters, outputFolder + singleFileName + "_clusters.png");
            ++counter;
        }
    }
}
